# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Environment variables (ENV['...']) are set in the file config/application.yml.
# See http://railsapps.github.com/rails-environment-variables.html
puts 'ROLES'
YAML.load(ENV['ROLES']).each do |role|
  Role.find_or_create_by_name({ :name => role }, :without_protection => true)
  puts 'role: ' << role
end
puts 'DEFAULT USERS'
user = User.find_or_create_by_email :name => ENV['ADMIN_NAME'].dup, :email => ENV['ADMIN_EMAIL'].dup, :password => ENV['ADMIN_PASSWORD'].dup, :password_confirmation => ENV['ADMIN_PASSWORD'].dup
puts 'user: ' << user.name
user.add_role :admin

Article.create(:title => "Article1", :content => "Some content", :user_id => 1)
Article.create(:title => "Article2", :content => "Some content", :user_id => 1)
Article.create(:title => "Article3", :content => "Some content", :user_id => 1)
Article.create(:title => "Article4", :content => "Some content", :user_id => 1)

Comment.create(:title => "Comment1", :body => "A kind of", :user_id => 1, article_id: 1)
Comment.create(:title => "Comment1", :body => "A kind of", :user_id => 1, article_id: 1)
Comment.create(:title => "Comment1", :body => "A kind of", :user_id => 1, article_id: 2)
Comment.create(:title => "Comment1", :body => "A kind of", :user_id => 1, article_id: 3)
Comment.create(:title => "Comment1", :body => "A kind of", :user_id => 1, article_id: 4)