// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require_tree .
//= require bootstrap-button
//= require bootstrap-wysihtml5

$(function() {
	
	FB.init({
	  appId      : '124898237690089', // App ID
	  status     : true, // check login status
	  cookie     : true, // enable cookies to allow the server to access the session
	  xfbml      : true,  // parse XFBML
	  oauth      : true
	});

	$("#fblogin").click(function(){
	  var user_email;
	  var user_id;
	  FB.login(function(response) {
	    if (response.authResponse) {
	    	window.location.reload();
	      // FB.api('/me', function(response) {
	      //   user_email = response.email;
	      //   FB.api('/me/likes', function(r){
	      //     $.each(r.data, function(index, value) {
	      //       if (value.id == "332592775590"){
	      //         likes = 1;
	      //       }          
	      //     });
	      //     $.ajax({
	      //       type  : "get",
	      //       contentType: 'application/json; charset=UTF-8',
	      //       accept: 'application/json',
	      //       dataType: 'json',
	      //       cache : false,
	      //       url   : "/customer/facebook_login",
	      //       data  : {id:user_id, email:user_email, likes:likes, birthday:birthday, hometown:hometown},
	      //       dataType: "text",
	      //       success: function(data) {
	      //         window.location.reload();
	      //       }
	      //     });
	      //   });
	      // });
	    } 
	    else {
	      alert('User cancelled login or did not fully authorize.');
	    }
	    // user_id = response.authResponse.userID;
	   
	  }, { scope: 'email' });
	});

});