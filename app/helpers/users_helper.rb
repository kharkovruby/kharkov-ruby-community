module UsersHelper

  def user_avatar(user)
    if user.avatar.present? 
    	user.avatar.url(:thumb) 
    else
      # default_url = "#{root_url}/assets/noavatar.jpg"
	    gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
	    "http://gravatar.com/avatar/#{gravatar_id}.png?s=200&d=mm"
    end  
  end

end